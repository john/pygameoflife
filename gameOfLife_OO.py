#!/usr/bin/python

import pygame, sys, random
from pygame.locals import *

BACKGROUND_COLOR = (0, 0, 0)
CELL_COLOR = (100, 20, 100)
TICK = 30

def run():


    pygame.init()
    width = 1920
    height = 1080
    screen = pygame.display.set_mode((width, height), RESIZABLE)  
    clock = pygame.time.Clock()

    cellSize = 8
    grid = []

    for y in range(height / cellSize):
        grid.append([])
        for x in range(width / cellSize):
            grid[y].append(Cell((x, y), cellSize, grid, screen, False))

    for y in range(height / cellSize):
        for x in range(width / cellSize):
            grid[y][x].init()

    for i in range(8000):
        grid[random.randrange(0, height / cellSize)][random.randrange(0, width / cellSize)].alive = True


    pygame.display.toggle_fullscreen()

    while 1:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    pygame.quit()
                    sys.exit()
        for y in range(height / cellSize):
            for x in range(width / cellSize):
                grid[y][x].wasAlive = grid[y][x].alive
        for y in range(height / cellSize):
            for x in range(width / cellSize):
                grid[y][x].drawCirc()
                grid[y][x].applyRules()
                
                
        clock.tick(TICK)
        pygame.display.flip()

class Cell:
    def __init__(self, position, size, grid, display, alive):
        self.size = size
        self.position = position
        self.alive = alive
        self.wasAlive = alive
        self.grid = grid
        self.display = display
        self.rect = Rect((self.position[0] * size, self.position[1] * size), (size, size))

    def init(self):
        self.gridHeight = len(self.grid) - 1
        self.gridWidth = len(self.grid[0]) - 1


    def drawRect(self):
        if self.alive:
            color = CELL_COLOR
        else:
            color = BACKGROUND_COLOR
        self.display.fill(color, self.rect)

    def drawCirc(self):
        if self.alive:
            color = CELL_COLOR
        else:
            color = BACKGROUND_COLOR
        pygame.draw.circle(self.display, color, (self.position[0] * self.size, self.position[1] * self.size), self.size / 2)


    def applyRules(self):
        count = self.countConnected()

        if self.alive & (count < 2):
            self.alive = False
        if self.alive & (count > 3):
            self.alive = False
        if (not self.alive) & (count == 3):
            self.alive = True

    def countConnected(self):
        count = 0

        if self.position[1] > 0 :
            if self.position[0] > 0 :
                if self.grid[self.position[1] - 1][self.position[0] - 1].wasAlive:
                    count += 1
            if self.grid[self.position[1] - 1][self.position[0]].wasAlive:
                count += 1
            if self.position[0] < self.gridWidth:
                if self.grid[self.position[1] - 1][self.position[0] + 1].wasAlive:
                    count += 1

        if self.position[0] > 0 :
            if self.grid[self.position[1]][self.position[0] - 1].wasAlive:
                count += 1
        if self.position[0] < self.gridWidth:
            if self.grid[self.position[1]][self.position[0] + 1].wasAlive:
                count += 1

        if self.position[1] < self.gridHeight:
            if self.position[0] > 0 :
                if self.grid[self.position[1] + 1][self.position[0] - 1].wasAlive:
                    count += 1
            if self.grid[self.position[1] + 1][self.position[0]].wasAlive:
                count += 1
            if self.position[0] < self.gridWidth:
                if self.grid[self.position[1] + 1][self.position[0] + 1].wasAlive:
                    count += 1
        return count

if __name__ == '__main__': run()