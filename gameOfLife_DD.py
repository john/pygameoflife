#!/usr/bin/python

import pygame, sys, random
from pygame.locals import *

BACKGROUND_COLOR = (0, 0, 0)
CELL_COLOR = (100, 20, 100)
TICK = 2
width = 1920
height = 1080
cellSize = 16

def empty_grid(h, w):
    g = []
    for y in range(h):
        g.append([])
        for x in range(w):
            g[y].append(False)
    return g

def init_grid(h, w):
    grid = empty_grid(h, w);
    # populate grid
    for i in range(8000):
        grid[random.randrange(0, height / cellSize)][random.randrange(0, width / cellSize)] = True

    return grid;

def run():
    pygame.init()
    screen = pygame.display.set_mode((width, height), RESIZABLE)  
    clock = pygame.time.Clock()


    grid_width = width / cellSize
    grid_height = height / cellSize

    grid_curr = init_grid(grid_height, grid_width)
    pygame.display.toggle_fullscreen()

    while 1:
        grid_old = list(grid_curr)
        grid_curr = empty_grid(grid_height, grid_width)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    pygame.quit()
                    return
                elif event.key == K_r:
                    grid_old = init_grid(grid_height, grid_width) 

        for y in range(grid_height):
            for x in range(grid_width):
                # count connected
                count = 0 
                if y > 0 :
                    if x > 0 :
                        if grid_old[y - 1][x - 1]:
                            count += 1
                    if grid_old[y - 1][x]:
                        count += 1
                    if x < grid_width - 1:
                        if grid_old[y - 1][x + 1]:
                            count += 1
                if x > 0 :
                    if grid_old[y][x - 1]:
                        count += 1
                if x < grid_width - 1:
                    if grid_old[y][x + 1]:
                        count += 1

                if y < grid_height - 1:
                    if x > 0 :
                        if grid_old[y + 1][x - 1]:
                            count += 1
                    if grid_old[y + 1][x]:
                        count += 1
                    if x < grid_width - 1:
                        if grid_old[y + 1][x + 1]:
                            count += 1

                # apply rules
                if grid_old[y][x] & ((count == 2) | (count == 3)):
                    grid_curr[y][x] = True
                if grid_old[y][x] & (count < 2):
                    grid_curr[y][x] = False
                if grid_old[y][x] & (count > 3):
                    grid_curr[y][x] = False
                if (not grid_old[y][x]) & (count == 3):
                    grid_curr[y][x] = True

        for y in range(grid_height):
            for x in range(grid_width):
                if grid_curr[y][x]:
                    color = CELL_COLOR
                else:
                    color = BACKGROUND_COLOR
                pygame.draw.circle(screen, color, (x * cellSize + cellSize / 2, y * cellSize + cellSize / 2), cellSize / 2)

        clock.tick(TICK)
        pygame.display.flip()

if __name__ == '__main__': run()